# sN4Ke_c4Se Random Key Generator 🔑

```
                                      ___
                                   ➤-⎛- o⎞
  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \  ⎠
   sN4Ke_c4Se Random Key Generator    ⎠ /
  ⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎛ ⎛
  

```

## About

This script generates a string of random characters according to the selected type(s) and number of characters from the user.


## License 

You can use this script for noncommercial usage only, with attribution, following its [PolyForm Noncommercial License 1.0.0](https://polyformproject.org/licenses/noncommercial/1.0.0/)

## Requirements

This script does not use any third-party dependencies.

## Usage

1. This script can be run as is with default values or can be run with arguments and options. The default types of characters will be alphanumeric lowercase plus uppercase with symbols. The default string-length will be 24 characters. 

2. The length of characters can be added in option.

3. The type(s) of characters can be added in option.

4. The `-silent` option can be added to remove all script messages.

## Command-Line Usage & Options

##### In a terminal: 

`python3 [full_path_to_the_file: sc_key.py] [optional: number_of_characters] [optional: -option]`

##### Options available: 

`-l` for Lowercase: Selects only lowercase alphabetic characters

`-u` for Uppercase: Selects only uppercase alphabetic characters

`-n` for Numeric: Selects only numeric characters

`-s` for Symbols: Selects only symbols characters

`-E` for Easy to Read: Removes harder to distinguish characters  
 >This option removes: ```O 0 2 Z I l , ; : | \ / ' " . ` ( ) [ ] { }```

`-h` for Help: Displays program usage and exit

`-silent` for Silent Mode: Only displays the generated random key without any messages


>**NOTE:** The options `-l`, `-u`, `-n`, and `-s` can be mixed together.  
  For example, typing: `python3 sc_key.py -lun` will exclude all symbol characters.



