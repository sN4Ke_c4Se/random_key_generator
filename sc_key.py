#!/usr/bin/env python3
################################################################################
##                                                                            ##
##  RANDOM KEY GENERATOR                                                      ##
##                                                                            ##
################################################################################

################################################################################
## LICENSE:                                                                   ##
################################################################################

"""
This software is licensed under the PolyForm Noncommercial License 1.0.0

In summary:
1. You may use, copy, share, or modify this code as long you respect its
noncommercial license.

2. You may use, copy, share, or modify this code as long you attribute it to its
original licensor by mentioning one of the 4-line Required Notices stated below.

3. You must indicate in comment if you have modified this code from its original
form by selecting the second modified Required Notice instead of the first one.

4. You can read the full license text here:
https://polyformproject.org/licenses/noncommercial/1.0.0

Include the following notice if you have not modified this code:

    Required Notice: Copyright @sN4Ke_c4Se (snake_case_software[at]pm.me | snakecase.software)
    Original source: https://gitlab.com/sN4Ke_c4Se/random_key_generator
    Licensed under the PolyForm Noncommercial License 1.0.0
    (https://polyformproject.org/licenses/noncommercial/1.0.0)

If you have modified this code, include the following notice instead:

    Required Notice (modified): Copyright @sN4Ke_c4Se (snake_case_software[at]pm.me | snakecase.software)
    Modified from the original source: https://gitlab.com/sN4Ke_c4Se/random_key_generator
    Licensed under the PolyForm Noncommercial License 1.0.0
    (https://polyformproject.org/licenses/noncommercial/1.0.0)

"""

################################################################################
## DESCRIPTION:                                                               ##
################################################################################

"""
Version: 1.0 (2023-09-15)

This script generates a string of random characters of a chosen length.
By default, the random characters string includes alphanumeric (lowercase and
uppercase) with symbols, but options can be selected to restrict the characters
types.

USAGE: python3 sc_key.py [key length (integer)] [optional: -option]

If no number of characters is received in arguments, the script will ask the
user for input at the start. If silent mode is select and no number of
characters is received in arguments, the script will use the current default
number of characters (24).

OPTIONS AVAILABLE:

`-l` for Lowercase: Selects only lowercase alphabetic characters
`-u` for Uppercase: Selects only uppercase alphabetic characters
`-n` for Numeric: Selects only numeric characters
`-s` for Symbols: Selects only symbols characters
`-E` for Easy to Read: Removes harder to distinguish characters
     -> This option removes: O 0 2 Z I l , ; : | \ / ' " . ` ( ) [ ] { }

`-h` for Help: Display program usage and exit
`-silent` for Silent Mode: Only displays the generated random key

NOTE:
The options 'l', 'u', 'n', and 's' can be mixed together.
For example, typing: [python3 sc_key.py -lun] will exclude symbol characters.

"""


################################################################################
### ALL IMPORTS                                                              ###
################################################################################

import sys
import secrets


################################################################################
### ALL CONSTANTS                                                            ###
################################################################################

FILE_PY_MAIN = "sc_key.py"

## PROGRAM OPTION VARIABLES: ###################################################
OP_LOWER = "lowercase"
OP_UPPER = "uppercase"
OP_NUMS = "numeric"
OP_SYMB = "symbols"
OP_EASY = "easy"
OP_SILENT = "silent"
OP_SILENT = "help"
OP_LOWER_CLI = "l" ## <- Only lowercase alphabetic characters
OP_UPPER_CLI = "u" ## <- Only uppercase alphabetic characters
OP_NUMS_CLI = "n" ## <- Only numeric characters
OP_SYMB_CLI = "s" ## <- Only symbols characters
OP_EASY_CLI = "E" ## <- Only easy to read and distinguished characters
OP_SILENT_CLI = "-silent" ## <- No messages or errors are printed
OP_HELP_CLI = "-h" ## <- Print a usage help message
OP_VALID = [OP_LOWER_CLI, \
            OP_UPPER_CLI, \
            OP_NUMS_CLI, \
            OP_SYMB_CLI, \
            OP_EASY_CLI]

## DEFAULT VARIABLES: ##########################################################
KEY_MIN_SUGGESTED = 16
KEY_DEFAULT_LEN = 24
KEY_DEFAULT_OP = [OP_LOWER_CLI, OP_UPPER_CLI, OP_NUMS_CLI, OP_SYMB_CLI]

## CHARACTER LISTS: ############################################################
KEY_ALPHA_LOWER = list("abcdefghijklmnopqrstuvwxyz") ## Option [-l]
KEY_ALPHA_UPPER = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ") ## Option [-u]
KEY_NUMERIC = list("0123456789") ## Option [-n]
KEY_SYMBOLS = list("!@#$%^&*()_+-=~`|[]:;'<>?,.") ## Option [-s]
KEY_EASY_EXCLUDE = list("O02ZIl,;:|\\/\'\".`()[]{}") ## Option [-E]
KEY_DEFAULT = KEY_ALPHA_LOWER + KEY_ALPHA_UPPER + KEY_NUMERIC + KEY_SYMBOLS
KEY_CHAR_DICTIONARY = {
                       OP_LOWER_CLI: KEY_ALPHA_LOWER, \
                       OP_UPPER_CLI: KEY_ALPHA_UPPER, \
                       OP_NUMS_CLI: KEY_NUMERIC, \
                       OP_SYMB_CLI: KEY_SYMBOLS\
                       }

## MESSAGE TYPES (with style and color): #######################################
STR_ERROR = "  \033[0;1;31mERROR!-> \033[0m" ## <- Red Bold
STR_NOTICE = "  \033[39;1;49mNOTICE-> \033[0m" ## <- Bold
STR_INPUT = "\n  \033[0;1;33mREQUIRED USER INPUT: \033[0m\n  " ## <- Yellow Bold
STR_WARNING = "  \033[0;1;33mWARNING!-> \033[0m" ## <- Yellow Bold

## GENERAL MESSAGES: ###########################################################
MSG_INPUT_LEN_IN = STR_INPUT + \
"Enter the desired characters-length for the key: "
MSG_NO_OPTION = STR_NOTICE + \
"No valid options were selected. The default type will be generated."
MSG_HELP_CONTINUE = "  Restart the program without the help option to continue.\n"

## PROGRAM USAGE: ##############################################################
easy_to_read = " ".join(KEY_EASY_EXCLUDE)
MSG_USAGE = \
f"\n  \033[39;1;49mUSAGE: python3 [full_path_to_the_file `{FILE_PY_MAIN}`] [key length (integer)] [optional: -option]\033[0m\n" \
+ f"\n  OPTIONS AVAILABLE:   `-{OP_LOWER_CLI}` for Lowercase: Selects only lowercase alphabetic characters" \
+ f"\n                       `-{OP_UPPER_CLI}` for Uppercase: Selects only uppercase alphabetic characters" \
+ f"\n                       `-{OP_NUMS_CLI}` for Numeric: Selects only numeric characters" \
+ f"\n                       `-{OP_SYMB_CLI}` for Symbols: Selects only symbols characters" \
+ f"\n                       `-{OP_EASY_CLI}` for Easy to Read: Removes harder to distinguish characters" \
+ f"\n                            -> This option removes: {easy_to_read} \n" \
+ f"\n                       `{OP_HELP_CLI}` for Help: Display program usage and exit" \
+ f"\n                       `{OP_SILENT_CLI}` for Silent Mode: Only displays the generated random key" \
+ f"\n\n  NOTE: " \
+ f"\n  The options '{OP_LOWER_CLI}', '{OP_UPPER_CLI}', '{OP_NUMS_CLI}', and '{OP_SYMB_CLI}' can be mixed together." \
+ f"\n  For example, typing: [python3 {FILE_PY_MAIN} -{OP_LOWER_CLI}{OP_UPPER_CLI}{OP_NUMS_CLI}] will exclude symbol characters.\n"

## WARNING_ MESSAGES: ##########################################################
WARN_SHORT_KEY = STR_WARNING + \
"This key is very short, be careful when using this key for a password."

## ERROR MESSAGES: #############################################################
ERR_MULTIPLE_LEN = STR_ERROR + \
"Only one length can be selected per key. Only the first number will be used."
MSG_NO_OPTIONS_F = STR_ERROR + \
"The option '{}' is an invalid option. Verify all valid options start with '-'."
ERR_INVALID_INT = STR_ERROR + \
"The value entered is not a valid integer. Enter an integer only."


################################################################################
### ALL FUNCTIONS                                                            ###
################################################################################

def ask_user_input_length():
    """Return a valid integer from user input."""

    while True:
        try:
            return int(input(MSG_INPUT_LEN_IN))
        except ValueError:
            print(ERR_INVALID_INT)

def compose_characters_list(options):
    """Return a list of characters following the selected program options."""

    characters = KEY_DEFAULT
    char_options = [opt for opt in options if opt != OP_EASY_CLI]
    if char_options:
        characters = []
        for opt in char_options:
            characters += KEY_CHAR_DICTIONARY[opt]
    if OP_EASY_CLI in options:
        characters = [c for c in characters if c not in KEY_EASY_EXCLUDE]
    return characters

def get_key_length(key_len):
    """Return a key length value (int). Return default value if none received in
    arguments and silent mode if selected, else ask user and Return valid
    input (int)."""

    if key_len == 0 and silent == True:
        return KEY_DEFAULT_LEN
    elif key_len == 0 and silent == False:
        return ask_user_input_length()
    return key_len

def generate_random_key(key_len, characters):
    """Return a random string of characters from the length and options
    selected."""

    key = ""
    for c in range(key_len):
        key += secrets.choice(characters)
    return key

def is_integer(arg):
    """Return `True` if arg is an integer, `False` if not."""

    try:
        int(arg)
        return True
    except ValueError:
        return False

def is_silent_mode(args):
    """Return `True` is the silent option was selected, else Return `False`."""

    if OP_SILENT_CLI in args:
        return True
    return False

def print_program_usage_and_exit(args):
    """If the help option was selected in arguments, print the program usage and
    exit."""

    if OP_HELP_CLI in args:
        print(MSG_USAGE)
        sys.exit(MSG_HELP_CONTINUE)

def print_verbose_messages(*messages):
    """Print a message unless the Silent Mode was selected."""

    if silent == False:
        for message in messages:
            print(message)

def validate_args(args):
    """Return a valid key length (int) and valid option(s) (str), if any."""

    args = args[1::]
    key_len = 0
    options = []
    ## Remove silent flag if in option:
    if OP_SILENT_CLI in args:
        args = [arg for arg in args if arg != OP_SILENT_CLI]
    ## Validate each arg in args (integer or valid option):
    for arg in args:
        if is_integer(arg) and key_len == 0:
            key_len = int(arg)
        elif is_integer(arg) and key_len != 0:
            print_verbose_messages(ERR_MULTIPLE_LEN)
        else:
            try:
                option = validate_option(arg)
                options += option
            except TypeError:
                print_verbose_messages(MSG_NO_OPTIONS_F.format(arg))
    return key_len, list(set(options))

def validate_option(arg):
    """Return valid option(s) if any, else return an empty list or `None`."""

    option = []
    if arg[:1] == "-":
        for c in arg[1:]:
            if c in OP_VALID:
                option.append(c)
        return option
    return None


## PROGRAM MAIN: ###############################################################

def main():
    """Generate and print a random string of characters of a selected length,
    composed of selected type(s) of characters."""

    ## If the help option is selected, print usage and exit program:
    print_program_usage_and_exit(sys.argv)

    ## Collect options and key length from arguments:
    global silent
    silent = is_silent_mode(sys.argv)
    key_len, options = validate_args(sys.argv)

    ## Ask for user input if no integer were passed in arguments (or default):
    key_len = get_key_length(key_len)

    ## Notice default option if no option were passed in arguments:
    if not options:
        print_verbose_messages(MSG_NO_OPTION)

    ## Display warning if key length selected is short:
    if key_len < KEY_MIN_SUGGESTED:
        print_verbose_messages(WARN_SHORT_KEY)

    ## Generate and print random key:
    characters = compose_characters_list(options)
    key = generate_random_key(key_len, characters)
    print_verbose_messages("")
    print(key + "\n")


################################################################################

if __name__ == "__main__":
    main()
